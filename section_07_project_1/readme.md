# README #

## course sections overview ##
--------
### how to play the game 
> the game is under mile_stone_project and will run in the terminal
> the game then will ask you which 'character you will like to be' x or o `(no capitals)`
> then it will let player to go first so if you picked 'x' you will be going second 
> there is no ai in this game it should be played with two people
> it will ask you to pick a number a number between 1 and 9 (this is where you would like the x or o to go)

|      |            |   |
| ------- |:------:| -----:|
|7   | 8| 9|
| 4| 5    |  6|
| 1|2    |    3 |




  >this is what the table will look like with the inputs your given 

> after someone wins the game it will ask if you would like to play again (its a yes or no question)
> then the cycle goes around again is put in 'yes' or 'no' if you dont want to play anymore

-----------

* What is this repository for?

>Milestone project with a few coding tests and a tic tac toe game 
> i did have problems doing this on my own so i went through the solutions videos and also the notebook that is provided to get more of an idea of how it works and what is going on

------------

* How do I get set up?

> the mile_stone_project will run without any input before playing the game and it will pop up in the terminal
> the other bits of code are just walk through's and tests so some of them might not work its only put in here for progress 

-----------

* Contribution guidelines

> test have been done for all coding in this section
>>  the milestone project works and also the displaying information works but the other two don'nt unfortunately 
> `version 1.0.2`
>> any changes with code or notes will be updates and the versions will be changed

----------------

### Who do I talk to? ###

` Reece wilson `
`reecewilson1298@gmail.com`