'''
Section 06
Coding Exercise 18:
Functions 
Examples: takes in a number of arguments and returns numbers that are even
works
'''




def check_even_list(num_list):
    
    even_numbers = []
    
    # Go through each number
    for number in num_list:
        # Once we get a "hit" on an even number, we append the even number
        if number % 2 == 0:
            even_numbers.append(number)
        # Don't do anything if its not even
        else:
            pass
    # Notice the indentation! This ensures we run through the entire for loop    
    return even_numbers

print (check_even_list([1,2,3,4,5,6]))