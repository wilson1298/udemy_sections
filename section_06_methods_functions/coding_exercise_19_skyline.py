
'''
Section 06
Coding Exercise 19:
Functions 
Examples: takes ina word and returns a matching string where the even numbers are uppercase
and the odd letters are lower case

# i had help from google for this one

'''




def myfunc(st):

    res = []

    for index, c in enumerate(st):
        if index % 2 == 0:
          
            res.append(c.upper())
        else:
            res.append(c.lower())

   
    return ''.join(res)
print(myfunc('helloworld'))
