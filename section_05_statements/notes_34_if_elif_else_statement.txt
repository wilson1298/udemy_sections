- what is control flow 
    - often only want certain code to execute when a particular condition has been met 
    - for example, if my dog is hungry (some condition), then i will feed the dog (some action)

- to control this flow of logic we use some keywords:
    - if
    - elif
    - else

- control flow syntax make use of colons and indentations (whitespace)

- this indentation system is crucial to python and is what sets it apart


- syntax of an if statement 
    - if some_condition:
        # execute some code
      elif: some_other_condition
        # do something different 
      else:
        # do something else 
      elif:

# coding examples

loc = 'Auto Shop'

if loc == 'Auto Shop':
    print("Cars are cool!")
elif loc == 'Bank':
    print("Money is cool")
elif loc == 'Store':
    print("Welcome to the store!")
else:
    print("I do not know much.")


# example 2

name = 'sammy':

    if name = 'frankie'
        print('hello frankie')

    elif name = 'sammy'
         print ('hello sammy')

    else:
        print("what is your name?")