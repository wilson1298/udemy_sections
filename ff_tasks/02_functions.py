# ================================================
# Here are some little exercises to help with the
# understanding with functions and arguments.
#
# Topic: Returns
#
# Marco | 4th May 2022
# ================================================


# Let's continue building a dog because it's fun.
# This time we want to add a pack of different dogs we can
# pick from.

# The pack members simply get a number. Then we look them up
# and return a small dictionary of information

import random


def pack(member):
    if member == 1:
        return {
            "color": "brown",
            "size": "small",
            "name": name(),
            "action" : action(0),
            "gender" : gender()
        }
    elif member == 2:
        return {
            "color": "black",
            "size": "medium",
            "name": name(),
            "action" : action(3),
            "gender" : gender()
        }
    elif member == 3:
        return {
            "color": "fox-red with white dots",
            "size": "huge",
            "name": name(),
            "action" : action(1),
            "gender" : gender()
        }
    else:
        return {
            "color": "undefinable",
            "size": "mysterious",
            "name": name(),
            "action" : action(),
            "gender" : gender()
        }

# Come back to this point after you've finished
# Exercise 2
# Use some of your past knowledge and new knowledge to solve this one.
# Create a collection of at least 10 different actions (action like in exercise 1).
# The action function should always return one action as a plain string.
# Once that's done, use this action function inside the dog function so that each dog
# does something different each time!

# Plan:
# List of action



def action(x=-1):
    #
    # Create a simple "list" of actions
    # Return one of these actions
    #
    
    
    actions = [
    "down at the pub with our gazza",
    "wanting something to eat",
    "liking the weather today",
    "doing nothing",
    "going to benidorm with the lads",
    "going fishing",
    "doing a pubcrawl",
    "just having a splendid time", 
    "not having a good day",
    "chilling with the boys"
    ]
    
    # for i in range(10):
    #     dog = random.choices(actions) 
    #     return dog[i]

    # Method B
    # i = random.randint(0,9)
    # return actions[i]('pack(member) == 2'):

    # # Method A

    print("X: {}".format(x))
    if x == -1:
        return random.choice(actions)
    else:
        return actions[x]


def gender():

    gender =["male", "female","unknown"]
    return random.choice(gender)

def name():

    name = [   'Chewbarka',
         'Fuzz Aldrin',
        'Hairy Paw-ter',
        'Kanye Westie',
         'Lick Jagger',
         'Snarls Barkley',
         'Boba Fetch',
         'toto africa',
         'Luke Skybarker',
         'Bark Wahlberg',
         'The Notorious D.O.G. ',
         'Ozzy Pawsborne',
         'Bark Obama',
         'Benedict Cumberbark',
 ]
    return random.choice(name)
     
# Inside the dog function we will receiver
# this dictionary of information and use it for
# our dog.

def dog(info):
    if info["gender"] == 'male':
        pn = 'he'
    elif info['gender'] == 'female':
        pn = 'she'
    else:
        pn = 'it'
        
    print("{} is a {} {}, {} dog and {} is {}".format(
        info["name"],
        info["gender"],
        info["size"],
        info["color"],
        pn,
        info["action"]
        
       ))
    # print("   __      _\n o'')}____//\n `_/      )\n (_(_/-(_/\n")
# We retrieve the info for member 1
# and store it in a new variable
# And then send it to the dog function
dog(pack(1))
dog(pack(2))
dog(pack(3))
dog(pack(4))
# # And then send it to the dog function

# We could do the same in one line like so:
###1) List of actions inside action()
###2) return one action 
####3) print that action to test it
###4) Add this now to the dog() function

