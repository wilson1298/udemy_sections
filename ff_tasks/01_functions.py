# ================================================
# Here are some little exercises to help with the
# understanding with functions and arguments.
# Marco | 4th May 2022
# ================================================


# Let's define a very basic function
# this one will "create" a dog
def dog():
    # Print a dog
    # This could stand for anything really -
    # physical print, starting a robot dog, some fancy app etc.
    print ("A dog")


# and we run it...
dog()

# NOTICE: usually we shouldn't have a function of the same name
# but in this case we a very simple example and the function will keep getting
# overridden on the way down... so just this time!

# Now let's give the dog an attribute
# We want to modify the colour of the dog, so..


def dog(color):
    print ("A {} dog.".format(color))


# again we run it..
dog("brown")
# and a different dog
dog("white")
# notice how we re-use our function above in different ways?
dog("blue and green")


# We want more though!
def dog(color, size):
    print ("A {}, {} dog.".format(size, color))


dog("brown", "large")
dog("white", "small")
dog("pink", "gigantic")

# Still not happy ?
# Let's make the the dogs bark..


def dog(color, size, eats, barks):
    print ("A {}, {},{} dog.".format(size, color,eats))
    for i in range(0, barks):
        print ("Woof")


dog("brown", "large","eating treats" ,3)
dog("white", "small", "eating a sandwich",2)
dog("pink", "gigantic","not hungry" ,10)



# Exercise 1
# Next - Give the dog and action, for example:
# "runs in the field" or "eats a snack"
# and add it to the above example.