import time

#  ============================
#    Quiz Application
#  ============================
def quiz():
    print('great! we will start now!\n')
    
    time.sleep(1) # adds in a timer so the writting can be spaced

   
    print('we will give you 5 random questions and we will tell you your score at the end of the game\n')
    time.sleep(1)
    print('GOODLUCK, you will need it\n')
    
    
    qs= {'In which part of your body would you find the cruciate ligament?\n':'knee',
    'How many permanent teeth does a dog have?\n':'42', 
    'At which venue is the British Grand Prix held?\n':'silverstone', 
    'Which European city hosted the 1936 Summer Olympics?\n':'berlin', 
    'EastEnders began broadcasting on BBC One in which year?\n':'1985'}
    
    key = list(qs)
    s = (key, 5) # only lets 5 questions go out for the user 

    for q in qs:
        
        score = 0
        for q,s in qs.items():
            
            if input(q).lower() == s.lower(): # allows the user to put in no capitals for the answer 
                score += 1
                print("Correct.")
            else:
                print('The answer was {}\n'.format(s)) # if wrong put answer in {} for the user to see 
        
        print('{}/5 Questions Correct'.format(score))
    
        again(quiz,calculate) 

# allows us to add time.sleep to allow different printing to be displayed at different times through out the code 
# ===================================
# calculator application  
# the easy version with only 2 inputs
# ===================================
def calculate():
    
    operation = input((
   ' Please type in the math operation you would like to complete:\n'  # allows the user to input what they want for the equation 
    '+ for addition\n'
    '- for subtraction\n'
    '* for multiplication\n'
    '/ for division\n'
    ))

    number_1 = int(input('Please enter the first number: ')) # at the moment only lets you put in two numbers but going to add more functions in the future
    number_2 = int(input('Please enter the second number: '))
    # addition
    if operation == '+':
        print('{} + {} = '.format(number_1, number_2))
        print(number_1 + number_2)
    # subtraction
    elif operation == '-':
        print('{} - {} = '.format(number_1, number_2))
        print(number_1 - number_2)
    # multiplication
    elif operation == '*':
        print('{} * {} = '.format(number_1, number_2))
        print(number_1 * number_2)
    # division
    elif operation == '/':
        print('{} / {} = '.format(number_1, number_2))
        print(number_1 / number_2)
    
    else:
        print('You have not typed a valid operator, please run the program again.')
    
    again(quiz,calculate) # allows the user to play again if needs be
    
#  ============================
#   MENU ITEMS / FUNCTIONS
#  ============================

def greet_user():
    # Greet user and stores name for future reference when you go back to the menu 

    user_name = str(input("enter the name : ")) 

    print("Hello {}, what's up?\n".format(user_name))

    
    return user_name

# main menu code
def again(quiz,calculate):
    choice = input( 
            ('Do you want to play again? '
            'Please type Y for YES or N for NO or M to go back to the menu: ')
        )
    while choice.upper() == 'Y': 
        print(quiz,calculate)
        for choice in quiz:
            return quiz()
            
    else:
        
        for choice in calculate:
            return calculate()
                   

    # allows user to play quiz again if in that function 

## menu return    
    while choice.upper() == 'M':
        print ('Going back to the menu now\n')
        time.sleep(1)
        menu()
### quit the game if wanting to       
    while choice.upper() == 'N':
        print ('okay see you later\n')
        quit()
 
 
 # Player 1

usr1 = greet_user() 

def menu():
    # again it greets the user and uses it in .format function and stores the name for future use 
    print('Where would my mate {} like to go today!\n'.format(usr1))
    
    choice = input("would you like to do a quiz (1) or go to the calculator (2): ")
    choice = choice.lower() # allows user to not use capitals if doesn't want to

    if choice in ['calculator', '2']:
        print('\nwe will now open the calculator!')
        
        # opens up calculator if entered 2 or calculator 
        calculate()

    elif choice in ['quiz', '1']:
        quiz()
        # opens up quiz if typed in quiz or 1 
    else:
        # if nothing that makes sense is typed in it prints this out:
        print("sorry, i didn't get that")
menu()